class SlotCollection < ApplicationRecord
    # Associations
    belongs_to :slot

    # Validations
    validates :start_time, presence: true
    validates :end_time, presence: true
    validates :capacity,presence: true, numericality: { only_numeric: true }, format: { with: /\A\d+\z/, message: "Integer only. No sign allowed." }
    validate :check_start_end_time
    
    # methods
    # checking the start and end time is valid or not
    def check_start_end_time
        if start_time.present? && end_time.present?
            errors.add(:start_time, "Start Time can't be in the past") if start_time < DateTime.now
            errors.add(:end_time, 'End Time is Invalid') if end_time < start_time
        end
    end

    # rs method
    def full_rs
        {
            id: id,
            start_time: start_time,
            end_time: end_time,
            capacity: capacity,
            slot_id: slot_id
        }
    end
end
