class Slot < ApplicationRecord
    # Associations
    has_many :slot_collections, dependent: :destroy

    # Validations
    validates :start_time, presence: true
    validates :end_time, presence: true
    validates :total_capacity,presence: true, numericality: { only_numeric: true }, format: { with: /\A\d+\z/, message: "Integer only. No sign allowed." }
    validate :check_start_end_time

    # Call backs
    after_save :create_slot_collections

    # Methods
    
    # checking the start and end time is valid or not
    def check_start_end_time
        if start_time.present? && end_time.present?
            errors.add(:start_time, "Start Time can't be in the past") if start_time < DateTime.now
            errors.add(:end_time, 'End Time is Invalid') if end_time < start_time
        end
    end

    # it will create the collections slots depends of capacity and start-end time
    def create_slot_collections
        self.slot_collections.destroy_all
        slot_collections = []
        start_time = self.start_time; end_time = self.start_time + 15.minutes; capacity = 0
        while self.end_time >= end_time
            slot_collections << {slot_id: self.id, start_time: start_time, end_time: end_time, capacity: 1, created_at: Time.now, updated_at: Time.now}
            capacity += 1; start_time, end_time = end_time, end_time + 15.minutes
            break if capacity > self.total_capacity
        end
        SlotCollection.insert_all(slot_collections)
        if self.total_capacity > capacity 
            self.slot_collections.order(end_time: :desc).map do |s|
                s.update!(capacity: s.capacity + 1)
                capacity += 1
                break if capacity == self.total_capacity
            end
        end
    end

    # rs method
    def full_rs
        {
            id: id,
            start_time: start_time,
            end_time: end_time,
            total_capacity: total_capacity,
            slot_collections: slot_collections.order(start_time: :asc).map(&:full_rs)
        }
    end
end
