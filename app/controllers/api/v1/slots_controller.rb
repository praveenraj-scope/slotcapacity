class Api::V1::SlotsController < Api::V1::BaseController
    before_action :set_slot, only: [:update, :destroy]

    # create the new slot and their collections
    # POST /slots
    def create
        slot = Slot.new(permit_params)
        if slot.valid?
            slot.save!
            render json: {is_success: true, data: slot.full_rs, message: 'Successfully Created'}, status: 200
        else
            render json: {is_success: false, data: {}, message: slot.errors.full_messages}, status: 500
        end
    end

    # update the slots and collections
    # PUT /slots/:id
    def update
        if @slot.update(permit_params)
            render json: {is_success: true, data: @slot.full_rs, message: 'Successfully Updated'}, status: 200
        else
            render json: {is_success: false, data: {}, message: @slot.errors.full_messages}, status: 500
        end
    end

    # delete the specific slot with collections
    # DELETE /slots/:id
    def destroy
        @slot.destroy
        render json: {is_success: true, data: {}, message: 'Successfully Deleted'}, status: 200
    end

    # return the specific slot
    # GET /slots/:id
    def show
        render json: {is_success: true, data: @slot.full_rs, message: ''}, status: 200
    end

    # returns the list of all slots
    # GET /slots
    def index
        page = params.has_key?(:page)? params[:page].to_i  : 1
		page_size = params.has_key?(:page_size)? params[:page_size].to_i  : 20
		start_index = (page - 1) * page_size
		end_index = (page_size * page) - 1
        where_condition = []
        where_condition << "start_time >= '#{params[:start_time]}' AND end_time <= '#{params[:end_time]}'" if params[:start_time].present? && params[:end_time].present?
        where_condition << "total_capacity = #{params[:total_capacity]}" if params[:total_capacity].present?
        slots = Slot.where(where_condition.join(' AND '))
        data = {total_records: slots.length, data: slots[start_index..end_index].map(&:full_rs)}
        render json: {is_success: true, data: data, message: ''}, status: 200
    end

    private

    def permit_params
        params.require(:slot).permit(:start_time, :end_time, :total_capacity)
    end

    def set_slot
        @slot = Slot.find_by(id: params[:id])
        return render json: {is_success: false, data: {}, message: @slot.errors.full_messages}, status: 500 if @slot.nil?
    end
end
