Rails.application.routes.draw do
  root 'welcome#index'

  get 'welcome/index'
  namespace :api, defaults: {format: :json}  do 
    namespace :v1 do 
      resources :slots, except: [:new, :edit]
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
